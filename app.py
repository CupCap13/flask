from flask import Flask, render_template, url_for, request
import time

app = Flask(__name__)


@app.route('/')
@app.route('/home')
def index():
    return render_template("index.html")


@app.route('/about')
def about():
    return render_template('about.html')


@app.route("/time", methods=['GET'])
def getTime():
    print("browser time: ", request.args.get("time"))
    print("server time : ", time.strftime('%A %B, %d %Y %H:%M:%S'));
    return render_template('time.html')


if __name__ == "__main__":
    app.run(debug = True) # app - Запуск локального сервера; debug=True извещение об ошибках в браузере (fals не будет видно ошибок)